/**
 * @license Copyright (c) 2014-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor.js';
import AutoLink from '@ckeditor/ckeditor5-link/src/autolink.js';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold.js';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials.js';
import Image from '@ckeditor/ckeditor5-image/src/image.js';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic.js';
import Link from '@ckeditor/ckeditor5-link/src/link.js';
import List from '@ckeditor/ckeditor5-list/src/list.js';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph.js';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import excerptIcon from './excerpt.svg';

class Excerpt extends Plugin {
	init() {
		const editor = this.editor;

		editor.ui.componentFactory.add('excerpt', locale => {
			const view = new ButtonView(locale);
			view.set({
				label: 'Excerpt',
				tooltip: true,
				icon: excerptIcon,
			});
			view.on('execute', () => {
				editor.model.change(writer => {
					// event that will be able to catch in the angular side
					window.dispatchEvent(new Event('excerpt-clicked'));
				});

			});

			return view;
		});
	}
}

class Editor extends ClassicEditor {}

// Plugins to include in the build.
Editor.builtinPlugins = [
	AutoLink,
	Bold,
	Essentials,
	Image,
	Italic,
	Link,
	List,
	Paragraph,
	Excerpt
];

// Editor configuration.
Editor.defaultConfig = {
	toolbar: {
		items: [
			'bold',
			'italic',
			'link',
			'bulletedList',
			'numberedList',
			'excerpt'
		]
	},
	language: 'en'
};

export default Editor;
